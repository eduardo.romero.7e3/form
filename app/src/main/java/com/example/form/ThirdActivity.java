package com.example.form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ThirdActivity extends AppCompatActivity {
    Button show;
    Button share;
    String nameString;
    boolean optionValue;
    int ageValue;
    String message;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        show = findViewById(R.id.show);
        share = findViewById(R.id.share);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            nameString = bundle.getString("name");
            optionValue = bundle.getBoolean("option");
            ageValue = bundle.getInt("age");
        }
        else Toast.makeText(getApplicationContext(), R.string.noData, Toast.LENGTH_SHORT).show();

        if(optionValue){
            message = "Hey, " + nameString + ", cómo llevas esos " + ageValue + "?";

        }
        else message = "Espero que nos veamos, " + nameString + ", antes de que cumplas " + (ageValue + 1);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra("string", message);

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });
    }
}
