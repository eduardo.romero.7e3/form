package com.example.form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    Button next;
    RadioGroup option;
    SeekBar ageBar;
    TextView age;
    String nameString;
    boolean optionValue;
    int ageValue;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        next = findViewById(R.id.next);
        option = findViewById(R.id.option);
        ageBar = findViewById(R.id.ageBar);
        age = findViewById(R.id.age);
        ageValue = Integer.parseInt(String.valueOf(ageBar.getProgress()));
        optionValue = true;

        age.setText(String.valueOf(ageBar.getProgress()));

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            nameString = bundle.getString("name");
        }
        else Toast.makeText(getApplicationContext(), R.string.noData, Toast.LENGTH_SHORT).show();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ageValue <= 60 && ageValue >= 18){
                    Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                    intent.putExtra("name", nameString);
                    intent.putExtra("option", optionValue);
                    intent.putExtra("age", ageValue);
                    startActivity(intent);
                }
                else Toast.makeText(getApplicationContext(), R.string.badAge, Toast.LENGTH_SHORT).show();
            }
        });

        option.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                optionValue = (checkedId == R.id.hi);
            }
        });

        ageBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ageValue = Integer.parseInt(String.valueOf(progress));
                age.setText(String.valueOf(ageValue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
}
